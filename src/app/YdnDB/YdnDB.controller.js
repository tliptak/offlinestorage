angular.module('dbs-app')
  .controller('YdnDBController', function ($scope, customersService, storageService, businessCenterService, testRepositoryService) {
    $scope.vm = this;
    this.title = 'Ydn-db';
    this.businessCenter = [];
    $scope.syncIntervalId = null;
    this.syncEnabled = false;
    this.gridOptions = {
      paginationPageSizes: [20, 100, 500, 1000],
      paginationPageSize: 20,
      data: 'vm.businessCenter',
      columnDefs: [
        { name: '_id' },
        { name: 'firstName' },
        { name: 'lastName' },
        {name: 'phone'},
        {name: 'email'},
        {name: 'modified'}
      ]
    };

    var worker = null;
    initWorker();

    refreshData();

    this.submitItem = function(){
      database.put(collectionName, {text:$scope.newItem}).then(function (){
        initializeData();
      });
    }


    this.removeItem = function(itemToRemove){
      database.remove(collectionName, itemToRemove.id);

    }

    this.filterData = function(){
      //var query = 'SELECT * FROM ' + collectionName + 'WHERE text = ' + $scope.vm.textFilter
      //database.executeSql(query).then(function (records){
      //  $scope.vm.filteredData = result;
      //});
      storageService.from('customers').where('firstName', '^', $scope.vm.textFilter).list(100).done(function (records) {
        $scope.$apply(function() {
          $scope.vm.filteredData = records;
        });
      });
    }

    this.synchronize = function(){
      customersService.loadCustomers();
    }

    function initWorker(){
      worker = new Worker('/app/sync/synchronize.js');
      worker.addEventListener('message', function(e) {
        if(e.data.cmd == 'syncCompleted'){
          refreshData();
          console.log('Message from worker: \n' + e.data.msg);
          toastr.success(e.data.msg, 'Message from worker: ')

        }else{
          console.log('Message from worker: \n' + e.data);
        }
      }, false);
    }

    this.synchronizeInWorker = function(){
      worker.postMessage({'cmd': 'start', 'msg': 'Hi'});
    }

    this.startAutoSync = function(){
      var syncInterval = 10000;
      $scope.syncIntervalId = setInterval(this.synchronizeInWorker, syncInterval);
      console.log('Auto sync every ' + syncInterval/1000 + ' seconds started.');
      $scope.vm.syncEnabled = true;
    };

    this.stopAutoSync = function(){
      window.clearInterval($scope.syncIntervalId);
      console.log('Auto sync stopped.');
      $scope.vm.syncEnabled = false;
    };

    this.init = function(){
      businessCenterService.init();
    }


    this.muddleUp = function(){
      var result = businessCenterService.muddleUp();
    }

    function refreshData() {
      //storageService.values('business-center').always(function(records) {
      //  $scope.$apply(function (){
      //    $scope.vm.businessCenter = records;
      //  })
      //});
      storageService.from('business-center').list(10000).done(function (records) {
        $scope.$apply(function () {
          $scope.vm.businessCenter = records;
        });
      });
    }

    this.fullTextSearch = function(){
      $scope.vm.filteredData = [];

      storageService.search('names', $scope.vm.fullTextFilter).done(function(result) {
        _.each(result, function(item){
          storageService.get(item.storeName, item.primaryKey).done(function(entry) {
            $scope.$apply(function () {
              $scope.vm.filteredData.push(entry);
            });
          })
        })

      });
    }

  });
