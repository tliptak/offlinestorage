angular.module('dbs-app')
  .factory('businessCenterService', function($http, storageService, _){
    var serviceUrl = 'http://localhost:3003/api/businessCenter';

    function init() {
      $http.get(serviceUrl + '/init').then(function (result) {
        //alert(result.data.status);
        toastr.success(result.data.status, 'Init result')
      });
    }

    function muddleUp() {
      $http.get(serviceUrl + '/muddleUp').then(function (result) {
        toastr.success(result.data.status, 'Mix up result')
      });
    }



    return {
      init: init,
      muddleUp: muddleUp
    }

  });
