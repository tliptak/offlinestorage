angular.module('dbs-app')
  .factory('storageService', function(){
    var dbName = 'vantage-mock';
    //enableLogging();
    var database = initializeDatabase();

    function enableLogging(){
      var module = 'ydn.db';
      var level = 'finer'; // warning, info, fine, finest, all
      ydn.debug.log(module, level);
    }

    function initializeDatabase() {
      console.log('[UI Thread]db inisialized');
      var db = new ydn.db.Storage(dbName, createSchema());

      db.addEventListener(['created', 'updated', 'deleted'], function(event) {
        console.log('[UI Thread]Event type : ' + event.type + ' store : ' + event.getStoreName());
      });

      //configure database
      return db;
    }

    function createSchema(){
      var schema = {
        fullTextCatalogs: [{
          name: 'names',
          lang: 'en',
          sources: [
            {
              storeName: 'customers',
              keyPath: 'firstName',
              weight: 1.0
            }, {
              storeName: 'customers',
              keyPath: 'lastName',
              weight: 1.0
            }]
        }],
        stores: [
          {
            name: 'customers',
            dispatchEvents: true,
            keyPath: '_id',
            indexes: [
              {name:'firstName'}
            ]
          },
          {
            name: 'business-center',
            dispatchEvents: true,
            keyPath: '_id',
            indexes: [
              {name:'firstName'}
            ]
          }
        ]
      }

      return schema;
    }

    return database;
  });
