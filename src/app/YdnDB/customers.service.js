angular.module('dbs-app')
  .factory('customersService', function($http, storageService, _){
    var serviceUrl = 'http://localhost:3003/api/customers';

    function loadCustomers() {
      $http.get(serviceUrl).then(function (result) {
        //storageService.putAll('customers', result.data).then(function () {
        //  alert('data synchronized');
        //});
        var count=0;
        var chunkedData = chunk(result.data, 1000);
        storeData(chunkedData, 0);
        //return;
        //_.each(result.data, function (item) {
        //  storageService.put('customers', item)
        //    .done(function(){
        //      count += 1;
        //      console.log(count + ' records added.');
        //    })
        //    .fail(function(error){
        //      console.log(error);
        //    })
        //});

      });
    }

    function storeData(chunks, i){
      if(i >= chunks.length){
        return;
      }
      storageService.put('customers', chunks[i])
        .done(function(){
          i++;
          console.log(i*1000 + ' records added. (' + (new Date()).toLocaleTimeString("en-US") + ')');
          storeData(chunks, i);
        })
        .fail(function(error){
          console.log(error);
        });
    }

    function chunk (array, size) {
      return array.reduce(function (res, item, index) {
        if (index % size === 0) { res.push([]); }
        res[res.length-1].push(item);
        return res;
      }, []);
    }

    return {
      loadCustomers: loadCustomers
    }

  });
