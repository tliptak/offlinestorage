var HumanaVantage = HumanaVantage || {};
HumanaVantage.TestRepository = function() {
  var items = [{id:1, name:'One'}, {id:2, name:'Two'}];

  this.getAll = function() {
    return items;
  };

  this.add = function(item) {
    items.push(item);
  };
};
