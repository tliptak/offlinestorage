var dbName = 'vantage-mock';
var database;

function getDatabase(){
  if(database === undefined) {
    database = initializeDatabase();
  }
  return database;
}


function initializeDatabase() {
  console.log('[Background Thread]db inisialized');
  var db = new ydn.db.Storage(dbName, createSchema());

  db.addEventListener(['created', 'updated', 'deleted'], function(event) {
    console.log('[Background Thread]Event type : ' + event.type + ' store : ' + event.getStoreName());
  });
  //configure database
  return db;
}

function createSchema(){
  var schema = {
    fullTextCatalogs: [{
      name: 'names',
      lang: 'en',
      sources: [
        {
          storeName: 'customers',
          keyPath: 'firstName',
          weight: 1.0
        }, {
          storeName: 'customers',
          keyPath: 'lastName',
          weight: 1.0
        }]
    }],
    stores: [
      {
        name: 'customers',
        dispatchEvents: true,
        keyPath: '_id',
        indexes: [
          {name:'firstName'}
        ]
      },
      {
        name: 'business-center',
        dispatchEvents: true,
        keyPath: '_id',
        indexes: [
          {name:'firstName'}
        ]
      }
    ]
  }

  return schema;
}
