importScripts('/app/vendor/ydn.db-i-core-e-qry-text.js',
  '/app/sync/utils.js',
  '/app/sync/db.js',
  '/app/sync/customers.js',
  '/app/sync/businessCenter.js',
  '/app/testEntity/test.repository.js');

var apiUrl = 'http://localhost:3003/api/';

self.addEventListener('message', function(e) {
  var data = e.data;
  var repo = new HumanaVantage.TestRepository();
  switch (data.cmd) {
    case 'start':
     // synchronizeCustomers();
      synchronizeBusinessCenter(repo);
      break;
    default:
      self.postMessage('Unknown command: ' + data.msg);
  };
}, false);


