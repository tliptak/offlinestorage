function synchronizeCustomers(){
  callAjax(apiUrl + 'customers' , function(data){

      var chunkData = chunk(data, 1000);
      storeData(chunkData, 0);
    }
  );
  self.postMessage('CUSTOMERS SYNCHRONIZATION STARTED');
}



function storeData(chunks, i){
  if(i >= chunks.length){
    self.postMessage('CUSTOMERS SYNCHRONIZATION COMPLETED');
    return;
  }
  getDatabase().put('customers', chunks[i])
    .done(function(){
      i++;
      self.postMessage(i*1000 + ' customers records loaded by Web Worker. (' + (new Date()).toLocaleTimeString("en-US") + ')');
      storeData(chunks, i);
    })
    .fail(function(error){
      console.log(error);
    });
}
