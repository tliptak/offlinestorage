var apiUrl = 'http://localhost:3003/api/';

function synchronizeBusinessCenter(repository){
  self.postMessage('SYNCHRONIZATION OF BUSINESS CENTER STARTED');
  callAjax(apiUrl + 'businessCenter', function(data){

        if(data.type == 'diffSync'){
          storePartialData(data.data, repository);
        }
        else{
          storeFullData(data.data.inserted, repository);
       //   storePartialData(data);
        }
    }
  );
}

function storeFullData(data, repository){
  getDatabase().clear('business-center');
  getDatabase().putAll('business-center', data)
    .done(function(){
      var records = repository.getAll();
      var msg = '[FULL] business center records loaded by Web Worker.' + records;
      self.postMessage({'cmd': 'syncCompleted', 'msg': msg});
    })
    .fail(function(error){
      console.log(error);
    });
}

function storePartialData(data, repository){
  var db = getDatabase();

  var inserted = data.inserted;
  var updated = data.updated;
  var deleted = data.deleted;

  if(inserted.length > 0){
    addRecords(db, inserted, repository);
  }
  if(updated.length > 0){
    updateRecords(db, updated);
  }
  if(deleted.length > 0){
    deleteRecords(db, deleted);
  }

  //self.postMessage('[PARTIAL] BUSINESS CENTER SYNCHRONIZATION COMPLETED');
  return;

}

function addRecords(db, data){
 // self.postMessage('[PARTIAL][ADD] STARTED');
  db.putAll('business-center', data)
    .done(function(d) {
      var msg = '[PARTIAL] business center records added : ' + d.length;
    //  self.postMessage('[PARTIAL] business center records added : ' + d.length);
      self.postMessage({'cmd': 'syncCompleted', 'msg': msg});
    })
    .fail(function(err){
      self.postMessage('[PARTIAL] add error :' + err);
    })
}

function deleteRecords(db, data)
{
  //self.postMessage('[PARTIAL][DELETE] STARTED');
  var keys = new Array();

  //db.count('business-center').done(function (r){
  //  var t = r;
  //})
  for(i=0; i<data.length; i++){
    keys.push(new ydn.db.Key("business-center", data[i]));
  }

  db.remove(keys).done(function(d) {
    var msg = '[PARTIAL] business center records deleted : ' + d;

    self.postMessage({'cmd': 'syncCompleted', 'msg': msg});
  }).fail(function(err){
      self.postMessage('[PARTIAL] delete error :' + err);
    })
}

function updateRecords(db, data){
 // self.postMessage('[PARTIAL][UPDATE] STARTED');

  //var updatedRecords = new Array();
  //for (i = 0; i < data.length-1; i++) {
  //  db.get('business-center', data[i]._id).done(function(result) {
  //    result.firstName = data[i].firstName;
  //    result.lastName = data[i].lastName;
  //    result.email = data[i].email;
  //    result.phone = data[i].phone;
  //    result.modified = data[i].modified;
  //
  //    updatedRecords.push(result);
  //  })
  //}

  db.putAll('business-center', data)
    .done(function(d) {
      var msg = '[PARTIAL] business center records updated : ' + d.length;
      self.postMessage({'cmd': 'syncCompleted', 'msg': msg});
    })
    .fail(function(err){
      self.postMessage('[PARTIAL] update error :' + err);
    })
}
