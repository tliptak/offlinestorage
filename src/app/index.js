'use strict';

angular.module('dbs-app', ['ngRoute', 'ui.bootstrap', 'pouchdb', 'underscore', 'ui.grid', 'ui.grid.pagination'])
  .config(function ($routeProvider, $httpProvider) {
    //$httpProvider.defaults.useXDomain = true;
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $routeProvider
      //.when('/', {
      //  templateUrl: 'app/main/main.html',
      //  controller: 'MainCtrl'
      //})
      //.when('/ForerunnerDB', {
      //  templateUrl: 'app/ForerunnerDB/ForerunnerDB.html',
      //  controller: 'ForerunnerDBController'
      //})
      //.when('/PouchDB', {
      //  templateUrl: 'app/PouchDB/PouchDB.html',
      //  controller: 'PouchDBController'
      //})
      .when('/', {
        templateUrl: 'app/YdnDB/YdnDB.html',
        controller: 'YdnDBController'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
;
