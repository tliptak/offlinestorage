'use strict';

angular.module('dbs-app')
  .controller('MainCtrl', function ($scope) {
    $scope.vm = this;

    var originalData = [
      ['Artist', 'Album', 'Price'],
      ['Buckethead', 'Albino Slug', 8.99],
      ['Buckethead', 'Electric Tears', 13.99],
      ['Buckethead', 'Colma', 11.34],
      ['Crystal Method', 'Vegas', 10.54],
      ['Crystal Method', 'Tweekend', 10.64],
      ['Crystal Method', 'Divided By Night', 8.99]
    ];

    this.generateExcel = function(){
      var artistWorkbook = ExcelBuilder.createWorkbook();
      var albumList = artistWorkbook.createWorksheet({name: 'Album List'});
      var albumList = artistWorkbook.createWorksheet({name: 'Album List'});

      var albumTable = new Table();
      albumTable.styleInfo.themeStyle = "TableStyleDark2"; //This is a predefined table style
      albumTable.setReferenceRange([1, 1], [3, originalData.length]); //X/Y position where the table starts and stops.

      //Table columns are required, even if headerRowCount is zero. The name of the column also must match the
      //data in the column cell that is the header - keep this in mind for localization
      albumTable.setTableColumns([
        'Artist',
        'Album',
        'Price'
      ]);

      albumList.setData(originalData);
      artistWorkbook.addWorksheet(albumList);

      albumList.addTable(albumTable);
      artistWorkbook.addTable(albumTable);

      var data = ExcelBuilder.createFile(artistWorkbook, {type: "uint8array"});

      var blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      saveAs(blob, "sample.xlsx");

      //var dataArray = ExcelBuilder.createFile(testDataWorkbook, {type: "uint8array"});
      //var blob = new Blob([dataArray], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      //saveAs(blob, "test.xlsx");
    }

  });
