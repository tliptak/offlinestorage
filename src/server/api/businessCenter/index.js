var express = require('express');

var router = express.Router();
var controller = require('./businessCenter.controller');

router.get('/', controller.list);
router.get('/init', controller.init);
router.get('/muddleUp', controller.muddleUp);

module.exports = router;
