var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var customersSchema = new Schema({
  firstName:String,
  lastName: String,
  email: String,
  phone:String
});

module.exports = mongoose.model('customers', customersSchema);
