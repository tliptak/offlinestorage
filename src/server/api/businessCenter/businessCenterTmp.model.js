var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var businessCenterTmp = new Schema({
  firstName:String,
  lastName: String,
  email: String,
  phone:String
});

module.exports = mongoose.model('businessCenterTmp', businessCenterTmp);
