var utils = require('../.././utils');
var customers = require('./customers.model');
var businessCenter = require('./businessCenter.model');
var businessCenterTmp = require('./businessCenterTmp.model');

module.exports = (function(){

  var bcSize = 1000;
  var numberOfItemsToMix = 50;

  function list(request, response){
    console.log('get all');
    businessCenter.find(function(err,docs){
      if(err) {
        response.send(err);
        return;
      }
      if(docs.length == 0){
        writeFullSync(response);
      }
      else{
        writeDiffSync(docs, response)
      }
    });

  }
  function writeFullSync(response){
    console.log('full sync started');
    businessCenterTmp.find(function (err, clientData){
      businessCenter.create(clientData, function(err){
        console.log('records created');
        if(err){
          console.log(err);
          response.send(err);
        }
        else{
          response.send({
            type: 'fullSync',
            data:{
              inserted:clientData,
              updated: [],
              deleted: []
            }
          })
        }
      });
    });
  }

  function writeDiffSync(oldData, response) {
    console.log('diff sync started');
    businessCenterTmp.find(function (err, actualData) {
      if (err) {
        response.send(err);
      }
      else {
        var actualDataDictionary = {};
        var oldDataDictionary = {};
        for(var i=0; i<actualData.length; i++){
          actualDataDictionary[actualData[i]._id] = actualData[i];
        }

        var insertedData = [],
          updatedData = [],
          deletedData = [];

        for(var i=0; i<oldData.length; i++){
          var oldItem = oldData[i];
          var actualItem = actualDataDictionary[oldItem._id];
          oldDataDictionary[oldItem._id] = oldItem;

          if(actualItem === undefined){
            deletedData.push(oldItem._id);
          }
          else if(actualItem.firstName !== oldItem.firstName){
            updatedData.push(actualItem);
          }
        }

        for(var i=0; i<actualData.length; i++){
          if(oldDataDictionary[actualData[i]._id] === undefined){
            insertedData.push(actualData[i])
          }
        }

        businessCenter.remove({}, function(err){
          businessCenter.create(actualData, function(err) {
            response.send({
              type: 'diffSync',
              data: {
                inserted: insertedData,
                updated: updatedData,
                deleted: deletedData
              }
            })
          });
        });

      }
    });
  }

  function init(request, response) {
    businessCenterTmp.remove({}, function (e) {
      if (e) {
        response.send(e);
        return;
      }
      businessCenter.remove({}, function (e) {
        if (e) {
          response.send(e);
          return;
        }
        customers.find({}, {}, {limit: bcSize}, function (e, docs) {
          if (e) {
            response.send(e);
            return;
          }
          businessCenterTmp.create(docs, function (e) {

            if (e) {
              response.send(e);
              return;
            }
            response.send({status: "Initialized"});
          })
        });
      });
    });
  }

  function muddleUp(request, response){

//    response.send({abc: utils.randomIntFromInterval(2, 100)});
//return;

    var bucketStart = utils.randomIntFromInterval(2, 100) * bcSize;
    var generatedIds = {};
    var itemIdsToUpdate = generateNumbers(1, bcSize, numberOfItemsToMix, generatedIds),
      itemIdsToDelete = generateNumbers(1, bcSize, numberOfItemsToMix, generatedIds),
      itemIdsToAdd = generateNumbers(1, bcSize, numberOfItemsToMix, generatedIds);



    //get original items
    businessCenterTmp.find({}, {}, {}, function(e,bcDocs){
      var modifiedDate = new Date();
      var idsToRemove = []
      for(var ri=0; ri< itemIdsToDelete.length; ri++){
        idsToRemove.push(bcDocs[itemIdsToDelete[ri]]._id);

      }


      //get items from other bucket and mix with original
      customers.find({}, {}, { limit : bcSize, skip: bucketStart }, function(e,customersDocs){

        //update items
        var bulk = businessCenterTmp.collection.initializeOrderedBulkOp();

        for(var ri=0; ri< itemIdsToUpdate.length; ri++){
          var originalItem = bcDocs[itemIdsToUpdate[ri]];
          var newItem = customersDocs[itemIdsToUpdate[ri]];
          bulk.find({_id: originalItem._id}).updateOne({$set: {
            firstName: newItem.firstName,
            lastName: newItem.lastName,
            email: newItem.email,
            phone: newItem.phone,
            modified: modifiedDate
          }});
        }

        bulk.execute(function (e) {
          if(e) console.log(e);

          businessCenterTmp.remove({_id: {$in: idsToRemove}}, function (e){
            if(e) console.log(e);

            var itemsToInsert = [];
            for(var ri=0; ri< itemIdsToAdd.length; ri++){
              //console.log('customersDocs.lenght: ' + customersDocs.length);
              //console.log('itemIdsToAdd.lenght: ' + itemIdsToAdd.length);
              //console.log('ri: ' + ri);
              //console.log('itemIdsToAdd[ri]: ' + itemIdsToAdd[ri]);
              var itemToInsert = customersDocs[itemIdsToAdd[ri]];
              itemToInsert.modified = modifiedDate;
              itemsToInsert.push(itemsToInsert);
            }
            businessCenterTmp.create(itemsToInsert, function (e){
              if(e) console.log(e);
              else{
                response.send({status: "Mixed"});
              }
            });
          });
        });
      });


      //response.send(docs);
    });
  }

  function generateNumbers(min, max, itemsToGenerate, generated){
    var arr = [];
    for(var i = 0; i<itemsToGenerate; i++){
      var generatedNumber = utils.randomIntFromInterval(min, max) - 1;
      while(generated[generatedNumber] !== undefined){
        generatedNumber = utils.randomIntFromInterval(min, max) - 1;
      }
      generated[generatedNumber] = 1;
      arr.push(generatedNumber);
    }
    return arr;
  }

  return{
    list: list,
    init:init,
    muddleUp: muddleUp
  }
})();
