var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var businessCenter = new Schema({
  firstName:String,
  lastName: String,
  email: String,
  phone:String
});

module.exports = mongoose.model('businessCenter', businessCenter);
