var express = require('express');

var router = express.Router();
var controller = require('./customers.controller');

router.get('/', controller.list)

module.exports = router;