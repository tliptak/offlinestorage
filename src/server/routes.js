var path = require('path');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/customers', require('./api/customers'));
  app.use('/api/businessCenter', require('./api/businessCenter'));

};
