var express    =    require('express');
var app        =    express();


var mongoose = require('mongoose');
mongoose.connect('localhost:27017/vantage-mock', {
  options: {
    db: {
      safe: true
    }
  }
});

mongoose.connection.on('error', function(err) {
    console.error('MongoDB connection error: ' + err);
    process.exit(-1);
  }
);

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  next();
});



require('./routes')(app);
var server     =    app.listen(3003,function(){
  console.log("We have started our server on port 3003");
});
